﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace OCRDemo
{
    public class FacilityAppProcess
    {
        private static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);
        private static Android.App.Activity _context;

        public FacilityAppProcess(Android.App.Activity context)
        {
            _context = context;
        }

        public async Task RunTask(Func<Task> mainAction, Action<Exception> onError = null, Action onFinal = null, bool showLoading = true)
        {
            if (semaphoreSlim.CurrentCount == 1)
            {
                ShowLoadingDialog(showLoading);
                await semaphoreSlim.WaitAsync();

                _ = Task.Run(async delegate
                {
                    string token = string.Empty;
                    _context.RunOnUiThread(async () =>
                    {
                        try
                        {
                            await mainAction();
                        }
                        catch (Exception ex)
                        {
                            HideLoadingDialog(showLoading);
                            CallError(onError, ex);
                        }
                        finally
                        {
                            semaphoreSlim.Release();
                            HideLoadingDialog(showLoading);
                            CallFinal(onFinal);
                        }
                    });



                });
            }
        }

        private static void CallFinal(Action onFinal)
        {
            if (onFinal != default) onFinal();
        }

        private static void CallError(Action<Exception> onError, Exception ex)
        {
            if (onError != default) onError(ex);
        }

        private static void HideLoadingDialog(bool showLoading)
        {
            
        }

        private static void ShowLoadingDialog(bool showLoading)
        {
            
        }
    }
}
