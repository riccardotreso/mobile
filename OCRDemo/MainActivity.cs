﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Widget;
using Newtonsoft.Json;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace OCRDemo
{

    public class PredictionResult
    {
        public byte[] Image { get; set; }
        public double Probability { get; set; }
    }

    public class ComputerVisionResult
    {
        public PredictionResult Prediction { get; set; }
        public string Text { get; set; }
    }


    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView imageView;
        TextView textView;

        protected FacilityAppProcess facilityAppProcess;

        static string endpoint = "https://app-poc-atmocr-01.azurewebsites.net/api/ocrplate/image";

        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Button button = FindViewById<Button>(Resource.Id.btn_take_photo);
            button.Click += btn_takePhoto_click;

            imageView = FindViewById<ImageView>(Resource.Id.imageView);
            textView = FindViewById<TextView>(Resource.Id.txt_ocr_text);

            facilityAppProcess = new FacilityAppProcess(this);
        }


        private async void btn_takePhoto_click(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {

                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg",
                CompressionQuality = 50,
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Small
            });

            if (file == null)
                return;

            byte[] byteData = null;
            using (Stream stream = file.GetStream())
            {
                byteData = new byte[stream.Length];
                stream.Read(byteData, 0, (int)stream.Length);
            }

            imageView.SetImageURI(Android.Net.Uri.FromFile(new Java.IO.File(file.Path)));

            textView.Text = "Riconoscimento testo in corso .....";

            await facilityAppProcess.RunTask(async () =>
            {
                textView.Text = await ReadText(byteData);
            }, (ex) =>
            {
                textView.Text = ex.Message;
            });

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }



        /// <summary>
        /// Gets the text from the specified image file by using
        /// the Computer Vision REST API.
        /// </summary>
        /// <param name="imageFilePath">The image file with text.</param>
        public async Task<string> ReadText(byte[] byteData)
        {
            try
            {
                HttpClient client = new HttpClient();

                HttpResponseMessage response;
                IList<ComputerVisionResult> results;

                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response = await client.PostAsync(endpoint, content);
                    results = JsonConvert.DeserializeObject<IList<ComputerVisionResult>>(await response.Content.ReadAsStringAsync());
                }

                return string.Join(System.Environment.NewLine, results.Select(x => $"{x.Prediction.Probability * 100}% -> {x.Text}"));

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
